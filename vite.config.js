import { fileURLToPath, URL } from 'node:url'
import path from 'path'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons';

// https://vitejs.dev/config/
export default defineConfig({
  base: "/tdesgin-admin",
  plugins: [vue(), vueJsx(), createSvgIconsPlugin({
    // 指定要缓存的图标文件夹
    iconDirs: [path.resolve(process.cwd(), 'src/icons/svg')],
    // 执行icon name的格式
    symbolId: 'svg-icon-[name]',
  })],
  // 代理服务
  server: {
    port: 9527,
    host: true,
    open: true,
    proxy: {
      '/dev-api': {
        target: 'https://www.fastmock.site/mock/ea6454e7331317055e23fc0b77447da6/api',
        changeOrigin: true,
        rewrite: (p) => p.replace(/^\/dev-api/, '')
      }
    }
  },
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  css: {
    preprocessorOptions: {
      less: {
        math: 'always' // 括号内才使用数学计算
      }
    }
  }
})
