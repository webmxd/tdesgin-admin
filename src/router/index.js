import { createRouter, createWebHistory } from 'vue-router'
import Layout from '@/Layout/index.vue';
import ParentView from "@/components/ParentView/index.vue"
/**
 * Note: 路由配置项
 *
 * hidden: true                   // 当设置 true 的时候该路由不会再侧边栏出现 如403，login等页面，或者如一些编辑页面/edit/1
 * alwaysShow: true               // 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
                                  // 只有一个时，会将那个子路由当做根路由显示在侧边栏--如引导页面
                                  // 若你想不管路由下面的 children 声明的个数都显示你的根路由
                                  // 你可以设置 alwaysShow: true，这样它就会忽略之前定义的规则，一直显示根路由
 * redirect: noRedirect           // 当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
 * name:'router-name'             // 设定路由的名字，在<keep-alive>中开启缓存会用到，页面中的name也需要保持一致才会缓存起作用
 * meta : {
    keep-alive: true              // 如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
                                  // 注意该缓存设置开启tabsView标签页中才会起作用，普通点击左侧菜单是不会缓存的
    title: 'title'                // 设置该路由在侧边栏和面包屑中展示的名字
    subtitle: 'subtitle',         // 左侧菜单的子标题名称，仅展开时显示
    icon: 'svg-name'              // 设置该路由的图标，阿里云字体图标（icon-shu3-xianxing）建议使用阿里云字体图标
    breadcrumb: false             // 如果设置为false，则不会在breadcrumb面包屑中显示
    affix: true                   // 如果设置为true，它则会固定在tabs-view中(默认 false)
    target: ''                    // 链接或路由跳转方式。可选项：_blank/_self/_parent/_top
    activeMenu: '/article/list'   // 当路由设置了该属性，则会高亮相对应的侧边栏。
                                  // 这在某些场景非常有用，比如：一个文章的列表页路由为：/article/list
                                  // 点击文章进入文章详情页，这时候路由为/article/1，但你想在侧边栏高亮文章列表的路由，就可以进行如下设置
  }
 */
export const constantRoutes = [
  // 重定向路由
  {
    path: "/redirect",
    name: "redirect",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "/redirect/:path(.*)",
        component: () => import('@/views/redirect.vue')
      }
    ]
  },
  // 登录
  {
    path: "/login",
    name: "login",
    component: () => import('@/views/login/index.vue'),
    meta: {
      title: "登录",
    },
    hidden: true
  },
  // 401页面
  {
    path: "/401",
    name: "401",
    component: () => import("@/views/error/401.vue"),
    meta: {
      title: "401",
    },
    hidden: true
  },
  // 403页面
  {
    path: "/403",
    name: "403",
    component: () => import("@/views/error/403.vue"),
    meta: {
      title: "403",
    },
    hidden: true
  },
  // 500页面
  {
    path: "/500",
    name: "500",
    component: () => import("@/views/error/500.vue"),
    meta: {
      title: "500",
    },
    hidden: true
  },
  // 默认路由跳转到首页
  {
    path: "/",
    redirect: "/home/control",
    component: Layout
  },
  // 首页
  {
    path: "/home",
    name: "home",
    component: Layout,
    redirect: "/home/control",
    meta: {
      title: "主控台",
      icon: "choujiang-xianxing"
    },
    hidden: false,
    children: [
      {
        path: "/home/control",
        name: "home-control",
        component: () => import("@/views/home/control/index.vue"),
        meta: { title: "主控台", subtitle: "", affix: true, icon: "choujiang-xianxing" }
      }
    ]
  },
  // 工作空间
  {
    path: "/workSpace",
    name: "workSpace",
    component: Layout,
    redirect: "/workSpace/workbench",
    alwaysShow: true,
    meta: {
      title: "工作空间",
      icon: "zhengfangti-xianxing"
    },
    hidden: false,
    children: [
      {
        path: "/workSpace/workbench",
        name: "workSpace-workbench",
        component: () => import("@/views/workSpace/workbench/index.vue"),
        meta: { title: "工作台", subtitle: "", affix: false}
      },
      {
        path: "/workSpace/analyse",
        name: "workSpace-analyse",
        component: () => import("@/views/workSpace/analyse/index.vue"),
        meta: { title: "分析页", subtitle: "", affix: false}
      }
    ]
  },
  {
    path: "/:pathMatch(.*)*",
    component: () => import('@/views/error/404.vue'),
    meta: {
      title: "404",
    },
    hidden: true
  }
];
//创建路由实例
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: constantRoutes
})
export default router
