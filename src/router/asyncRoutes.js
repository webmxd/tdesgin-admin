/**
 * 异步路由列表，模拟后台加载菜单后生成的路由数据
 */
export const asyncRoutes = [
  // 缓存页面
  {
    path: "/cache",
    component: "Layout",
    redirect: "/cache/hasCache",
    meta: {
      title: "缓存页面",
      icon: "jutiweizhi2-mianxing"
    },
    alwaysShow: true,
    hidden: false,
    children: [
      {
        path: "/cache/hasCache",
        name: "cache-hasCache",
        component: "cache/hasCache/index",
        meta: {
          title: "有缓存页面"
        }
      },
      {
        path: "/cache/noCache",
        name: "cache-noCache",
        component: "cache/noCache/index",
        meta: {
          title: "无缓存页面"
        }
      }
    ]
  },
  
  // 列表页面
  {
    path: "/list",
    component: "Layout",
    redirect: "/list/base",
    hidden: false,
    meta: {
      title: "列表页面",
      icon: "liebiao3-mianxing"
    },
    alwaysShow: true,
    children: [
      {
        path: "/list/base",
        name: "list-base",
        component: "list/base/index",
        meta: {
          title: "基础列表"
        }
      },
      {
        path: "/list/card",
        name: "list-card",
        component: "list/card/index",
        meta: {
          title: "卡片列表"
        }
      },
      {
        path: "/list/search",
        name: "list-search",
        component: "list/search/index",
        meta: {
          title: "搜索列表"
        }
      }
    ]
  },

  // 表单页面
  {
    path: "/form",
    component: "Layout",
    redirect: "/form/base",
    hidden: false,
    meta: {
      title: "表单页面",
      icon: "duobianxing-mianxing"
    },
    alwaysShow: true,
    children: [
      {
        path: "/form/base",
        name: "form-base",
        component: "form/base/index",
        meta: {
          title: "基础表单"
        }
      },
      {
        path: "/form/dynamic",
        name: "form-dynamic",
        component: "form/dynamic/index",
        meta: {
          title: "动态表单"
        }
      },
      {
        path: "/form/step",
        name: "form-step",
        component: "form/step/index",
        meta: {
          title: "分布表单"
        }
      },
    ]
  },

  // 结果页面
  {
    path: "/result",
    component: "Layout",
    redirect: "/result/success",
    hidden: false,
    meta: {
      title: "结果页面",
      icon: "jutiweizhi2-mianxing"
    },
    alwaysShow: true,
    children: [
      {
        path: "/result/success",
        name: "result-success",
        component: "result/success/index",
        meta: {
          title: "成功页面"
        }
      },
      {
        path: "/fail/success",
        name: "fail-success",
        component: "result/fail/index",
        meta: {
          title: "失败页面"
        }
      }
    ]
  },

  // 多级菜单
  {
    path: "/multilevel",
    component: "Layout",
    redirect: "/multilevel/menu1",
    meta: {
      title: "多级菜单",
      icon: "fenlei1-mianxing"
    },
    alwaysShow: true,
    hidden: false,
    children: [
      {
        path: "/multilevel/menu1",
        name: "/multilevel-menu1",
        meta: {
          title: "菜单1"
        },
        component: "multilevel/menu1/index"
      },
      {
        path: "/multilevel/menu2",
        name: "/multilevel-menu2",
        meta: {
          title: "菜单2"
        },
        // 多级菜单，需要设置parent的组件为ParentView才可以
        component: "ParentView",
        children: [
          {
            path: "/multilevel/menu2/menu2-1",
            name: "multilevel-menu2-menu2-1",
            meta: {
              title: "菜单2-1"
            },
            component: "multilevel/menu2/menu2-1/index"
          },
          {
            path: "multilevel/menu2/menu2-2",
            name: "multilevel-menu2-menu2-2",
            meta: {
              title: "菜单2-2"
            },
            component: "multilevel/menu2/menu2-2/index"
          }
        ]
      },
      {
        path: "/multilevel/menu3",
        name: "multilevel-menu3",
        meta: {
          title: "菜单3"
        },
        // 多级菜单，需要设置parent的组件为"ParentView"才可以
        component: "ParentView",
        children: [
          {
            path: "/multilevel/menu3/menu3-1",
            name: "multilevel-menu3-menu3-1",
            meta: {
              title: "菜单3-1"
            },
            component: "multilevel/menu3/menu3-1/index"
          },
          {
            path: "/multilevel/menu3/menu3-2",
            name: "multilevel-menu3-menu3-2",
            meta: {
              title: "菜单3-2"
            },
            // 多级菜单，需要设置parent的组件为"ParentView"才可以
            component: "ParentView",
            children: [
              {
                path: "/multilevel/menu3/menu3-2-1",
                name: "multilevel-menu3-menu3-2-1",
                meta: {
                  title: "菜单3-2-1"
                },
                component: "multilevel/menu3/menu3-2-1/index"
              },
              {
                path: "/multilevel/menu3/menu3-2-2",
                name: "multilevel-menu3-menu3-2-2",
                meta: {
                  title: "菜单3-2-2"
                },
                component: "multilevel/menu3/menu3-2-2/index"
              }
            ]
          }
        ]
      }
    ]
  }
];
