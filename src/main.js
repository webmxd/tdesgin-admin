import { createApp } from 'vue'

import App from './App.vue'
import router from './router'

// 引入状态管理
import store from "./store";

// 引入TDesgin 组件库
import TDesign from 'tdesign-vue-next';
import 'tdesign-vue-next/es/style/index.css';

// 引入公共样式库
import '@/styles/index.less';

// 引入阿里云字体图标库
import '@/assets/iconfont/iconfont.css';

// 引入svg icon注册脚本
import "virtual:svg-icons-register"; 

// 导入路由守卫
import "./permission.js";

// 引入全局组件
import SvgIcon from "@/components/SvgIcon/index.vue";
import ColorIcon from "@/components/ColorIcon/index.vue";
import PageTitle from "@/components/PageTitle/index.vue";

const app = createApp(App);
app.use(router);
app.use(store);
app.use(TDesign);
app.component("SvgIcon", SvgIcon);
app.component("ColorIcon", ColorIcon);
app.component("PageTitle", PageTitle);
app.mount('#app');
