export default {
  /**
   * 页面标签的title
   */
  title: "后台管理系统",
  /**
   * 侧边栏菜单顶部标题，为空则只显示logo
   */
  sidebarLogoTitle: "TDesgin",
  /**
   * 布局设置
   */
  // 侧边栏收起的宽度，单位px，不可动态修改，需要和 styles/variables.less中 @base-sider-width 保持一致
  sideBarCollapsedWidth: "68px",
  // 侧边栏展开的宽度，单位px，不可动态修改，需要和 styles/variables.less中 @base-sider-width 保持一致
  sideBarExpandWidth: "250px",
  layout: {
    // 主题颜色，深海蓝（light），明亮黄（yellow），天空蓝（lightblue），淡然紫（purple），可爱粉（pink），超甜橘（orange），自然棕（brown），草色青（ching）
    themeName: "light",
    // 侧边栏主题色：深色主题dark，浅色主题light，默认深色dark
    sideTheme: "dark",
    // 是否固定顶栏
    fixedHeader: true,
    // 是否固定左侧栏
    fixedSidebar: true,
    // 是否显示左侧栏顶部的logo
    sidebarLogo: true,
    // 侧边菜单栏是否开启手风琴模式，开启后每次打开菜单只能打开一个
    menuAccordion: true,
    // 菜单折叠时，是否在子菜单显示父级菜单名称
    menuCollapseParentTitle: false,
    // 是否开启水印
    isWater: false,
    // 是否开启 tabsView
    tabsView: true,
    // tabsView标签显示图标
    tabsViewIcon: false,
    // 面包屑第一层是否显示首页，否则会以当前点击的菜单根目录显示在第一层
    rootHome: true,
    // 显示页脚
    footer: true
  },
  /**
   * 是否显示动态标题
   */
  showDynamicTitle: true,
  // 主题色彩
  themeColorList: [
    { title: '深海蓝', name: 'light', color: '#0052d9' },
    { title: '明亮黄', name: 'yellow', color: '#f3b814' },
    { title: '天空蓝', name: 'lightblue', color: '#0894fa' },
    { title: '淡然紫', name: 'purple', color: '#7a5af8' },
    { title: '可爱粉', name: 'pink', color: '#ef45b3' },
    { title: '超甜橘', name: 'orange', color: '#f54343' },
    { title: '草色青', name: 'ching', color: '#27ba93' }
  ]
};
